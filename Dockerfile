FROM "debian:latest"

RUN apt update && apt-get install --no-install-recommends -y \
    python3 \
    make \
    gcc-arm-none-eabi \
    libnewlib-arm-none-eabi

COPY convert_makefile.py /opt