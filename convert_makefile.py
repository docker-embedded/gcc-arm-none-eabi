# Description: Converts the Atmel Studio 7.0 auto-generated Makefile to a Linux friendly Makefile

import os
import ntpath
import sys
import re
import fileinput
import optparse
import logging

# Strings to be replaced
shellToken = ("SHELL := cmd.exe", "SHELL := /bin/bash")
mkdirToken = ("@echo Building file: $<", "@echo Building file: $<\n\tmkdir -p $(dir $@)")
versionToken = ("6.3.1", "$(shell arm-none-eabi-gcc -dumpversion)")
execSearchExp = r"C:\\Program Files \(x86\)\\Atmel\\Studio\\7\.0\\toolchain\\arm\\arm-gnu-toolchain\\bin\\.*\.exe"

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

    # Configure the option parser
    usage = "usage: %prog <TargetMakefile>"
    parser = optparse.OptionParser(usage)
    (options, args) = parser.parse_args()

    # Check the length of arguments
    if len(args) == 0:
        parser.print_help()
        sys.exit(1)
    if len(args) != 1:
        parser.error("Need to specify the Makefile")

    targetMakefile = args[0]

    # Check for file existence
    if (not os.path.isfile(targetMakefile)):
        sys.exit("Error: The following file cannot be found: \n%s." % targetMakefile)

    # Inform user that script has started
    print ("Starting script to convert Windows based Atmel Makefile to Linux based.")
    print("Target Makefile is: %s" % targetMakefile)

    # Find and replace Windows string with Linux strings
    for line in fileinput.FileInput(targetMakefile, inplace=1):
        # Replace "cmd.exe" with "/bin/bash"
        line = line.replace(shellToken[0], shellToken[1])
        # Replace hard coded compiler version "6.3.1" with the output of a shell command
        line = line.replace(versionToken[0], versionToken[1])
        # Add the "mkdir -p" command
        line = line.replace(mkdirToken[0], mkdirToken[1])
        # Remove all references to "$(QUOTE)"
        line = line.replace("QUOTE := \"", "")
        line = line.replace("$(QUOTE)", "")
        # Strip out windows path to compiler and call executable directly
        result = re.findall(execSearchExp, line)
        if result:
            execString = ntpath.splitext(ntpath.basename(result[0]))[0]
            line = line.replace(result[0], execString)
        sys.stdout.write(line) #stdout goes to file